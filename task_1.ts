interface Student{
    name: string;
    avgMark: number;
}

interface Statistics{
    avgMark: number;
    highestMark: string;
    lowestMark: string;
}

function getStatistics(marks: Student[]): Statistics { 
    let tmp = {avgMark:0} as Statistics;
    let highestMarkElement = marks[0];
    let lowestMarkElement = marks[0];
    marks.forEach((element)=>{
        tmp.avgMark += element.avgMark;
        if (element.avgMark > highestMarkElement.avgMark){
            highestMarkElement = element;
        }
        if (element.avgMark < lowestMarkElement.avgMark){
            lowestMarkElement = element;
        }
    })
    tmp.avgMark /= marks.length;
    tmp.highestMark = highestMarkElement.name;
    tmp.lowestMark = lowestMarkElement.name;

    return tmp;
}

const testMarks = [{
    name: 'Vasya',
    avgMark: 3.75
},{
    name: 'Lena',
    avgMark: 4.89
}]

console.log(getStatistics(testMarks));