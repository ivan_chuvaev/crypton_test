function getHolesCount(number : number){
    if(number == 0)
        return 1;
    let counter = 0;
    while(number != 0){
        let digit = (number % 10);
        if(digit == 8){
            counter +=2;
        }else if(digit == 0 || digit == 4 || digit == 6 || digit == 9){
            counter += 1;
        }
        number = Math.floor(number / 10);
    }
    return counter;
}

//console.log(getHolesCount(0));

let arr = [3,1,2,5,4,6,7,11,32,25, 0];
arr.sort((a,b)=>{
    let ha = getHolesCount(a);
    let hb = getHolesCount(b);
    if(ha > hb){
        return 1;
    }else if(ha < hb){
        return -1;
    }else{
        if(a > b)
            return 1;
        return -1;
    }
})

console.log(arr);